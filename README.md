## Media Ripper

#### API service to rip media, analyze it, transcode it, and provide collaborative service

## Features

- [x] Upload any media
- [x] Transcode media from one format to another
- [x] Share media publicly and privately
- [x] Dockerized
- [x] Logged locally
- [x] Deployed to server (DO)
- [ ] Logged Remotely
- [ ] Basic analytics
- [ ] Crashlytics

#### Overview

 - Application is bare-minimum. It is just an api service, and an abstracted wrapper around ffmpeg.
 - We use FastAPI as the backend framework for the following reasons,
    - It's a very minimal framework, not restricting in terms of what we can use. (Unlike other heavy-weights)
    - It's highly performant for API-specific use cases. Ours is one.
    - It's very pythonic and modern, and is very easy to maintain and avoids common pitfalls (Validation through pydantic is super comfortable)
    - Automatic documentation through swagger/openAPI spec is handy.


### Known pitfalls

 - We don't use any performant database yet. (We are using sqlite. And Peewee for the ORM. Both are fine for a proof-of-concept, but shouldn't be applied in production unless explicitly required)
 - Database architecture can be further improved. App doesn't have any migration support yet.
 - The app architecture can further be improved into layers. This makes it easier for handling layered bugs and analytical information. Layers enable fine-grained analytical information.



#### Design decisions

 - *Pendulum* instead of *datetime* lib
    - Pendulum, deep down is just a wrapper to datetime. High ease of use, and cleaner code. 
 - *structlog* vs *logging*
    - Those who have used logging proficiently, know the "needs" of logging module to be a little bit better. Structlog provides such an use case. Providing analytics by parsing the logs provided by structlog is a lot more easier than that of the logging module. We can use this for future crash analytics as well. 
 - *Poetry* vs *Pipenv* vs *Pip*
    - I have used all of these and more. For reproducibility purposes, both pipenv and poetry works great. I have personally faced a lot of issues with pipenv in production especially when dockerizing services (unavailability of module versions during dependency resolution is a major PITA) Poetry solves that. 
    - Poetry also enables exporting of reproducible builds into __requirements.txt__ which is a boon. 
 - *FastAPI* vs *Flask* or *Django*
    - I love both flask and fastapi. FastAPI has an edge over flask due to performance, modernity, documentations. Some of the high-throughput modern distributed services written in python use fastapi for api services. 
 - *Sqlite*
    - Not a fan of using sqlite for production even though it is capable. But for the mvp purposes, sqlite serves better. Reduces the overhead of handling a DB server. 😛
 - *Peewee* vs *SQL*
    - Unless we aim for optimizations on the db performance, ORMs are easier on the DX (developer experiences). Moreover, going for plain sql when sqlite is the database, is unnecessary premature optimization. 🤷

