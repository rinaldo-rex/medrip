from fastapi import FastAPI
from .routers.media_handler import media_router
from .routers.users import users_router
from dynaconf import Dynaconf
import structlog


import logging.config
import structlog
from logging.handlers import RotatingFileHandler


timestamper = structlog.processors.TimeStamper(fmt="%Y-%m-%d %H:%M:%S")
pre_chain = [
    # Add the log level and a timestamp to the event_dict if the log entry
    # is not from structlog.
    structlog.stdlib.add_log_level,
    timestamper,
]

logging.config.dictConfig({
        "version": 1,
        "disable_existing_loggers": False,
        "formatters": {
            "plain": {
                "()": structlog.stdlib.ProcessorFormatter,
                "processor": structlog.dev.ConsoleRenderer(colors=False),
                "foreign_pre_chain": pre_chain,
            },
            "colored": {
                "()": structlog.stdlib.ProcessorFormatter,
                "processor": structlog.dev.ConsoleRenderer(colors=True),
                "foreign_pre_chain": pre_chain,
            },
        },
        "handlers": {
            "default": {
                "level": "DEBUG",
                "class": "logging.StreamHandler",
                "formatter": "colored",
            },
            # "file": {
            #     "level": "DEBUG",
            #     "class": "logging.handlers.RotatingFileHandler",
            #     "maxBytes": 2000,
            #     "filename": "logs/medrip.log",
            #     "backupCount": 5,
            #     "formatter": "plain",
            # },
        },
        "loggers": {
            "": {
                "handlers": ["default"],
                "level": "DEBUG",
                "propagate": True,
            },
        }
})
structlog.configure(
    processors=[
        structlog.stdlib.add_log_level,
        structlog.stdlib.PositionalArgumentsFormatter(),
        timestamper,
        structlog.processors.StackInfoRenderer(),
        structlog.processors.format_exc_info,
        structlog.stdlib.ProcessorFormatter.wrap_for_formatter,
    ],
    logger_factory=structlog.stdlib.LoggerFactory(),
    wrapper_class=structlog.stdlib.BoundLogger,
    cache_logger_on_first_use=True,
)

logger = structlog.get_logger(__name__)

handler = RotatingFileHandler('logs/medrip.log', mode='w+', maxBytes=2000, backupCount=5)
logger.addHandler(handler)

logger.info("Instantiating FastAPI app")

app = FastAPI()

logger.debug("Configuring the settings for dynaconf app")
settings = Dynaconf(
    envvar_prefix="DYNACONF",
    settings_files=['../settings.toml', '.secrets.toml'],
)

logger.debug("Adding routers for users and media")
app.include_router(media_router)
app.include_router(users_router)


def main():
    # This is just a dummy message. Don't worry about it. 😄
    return {"message": "Hello, world!"}
