"""
Creating the data directory as a package so we can directly import the data created over here. 😉
"""
from dataclasses import dataclass
from typing import List


@dataclass
class User:
    name: str
    email: str
    password: bytes

    # By default a created user is always active. Unless specified otherwise.
    is_active: bool = True


SAMPLE_USERS = [
    User(name="Rinaldo Rex", email="rinaldorexg@gmail.com", password="password"),
    User(name="Ashutosh", email="ashu@spext.co", password="password"),
    User(name="Sherlock Holmes", email="sherlockholmes@gmail.com", password="password"),
    User(name="John Watson", email="johnwatson@gmail.com", password="password"),
]
