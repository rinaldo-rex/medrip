from fastapi import File, Request, HTTPException
from pathlib import Path
import pendulum
import shutil
import bcrypt
import jwt
import structlog

from jwt import InvalidSignatureError
from dynaconf import settings
from fastapi.security import HTTPBearer, HTTPAuthorizationCredentials

from .ffmpeg_helper import probe_file


logger = structlog.getLogger(__name__)

def humanize_timestamp(timestamp):
    """Uses Pendulum to generate a human-readable timestamp

    Args:
        timestamp (str): Timestamp created by Pendulum. Or Datetime module
    """
    try:
        return pendulum.from_timestamp(timestamp).to_formatted_date_string()
    except TypeError as e:
        # When the timestamp is of invalid type, this should be gracefully ignored
        logger.error(f"Invalid timestamp provided: {timestamp}")
        return None


def humanize_size(bytes, units: str = "MB"):
    """Return a human-readable representation of bytes

    Args:
        bytes (int): Total bytes to be converted to other units
        units (str, optional): Returns the size in this unit. Defaults to 'MB'.
    """

    # Helper function was inspired by https://stackoverflow.com/a/52684562

    bytes = int(bytes)
    size_ = None
    if units == "MB":
        size_ = f"{'{:.2f}'.format(bytes/float(1<<20), 2)} MB"
    elif units == "kbps":
        size_ = f"{'{:.2f}'.format(bytes/1000, 2)} kb/s"
    else:
        logger.error(f"Unsupported unit provided: {units}")

    # TODO: Other units. And also raise appropriate exceptions
    return size_


def save_file(file: File, filename: str = None, save_dir: str = "uploads"):
    """Saves the given file/file-like (as with fastapi's async spooled file) onto the disk.


    Args:
        file (File): File or File like object to be saved
        filename (String): If given, uses this as the filename. Else uses the original filename.
        save_dir (String): Defaults to the configured save directory, else saves to the given dir.
    """
    fname = filename if filename else file.filename
    fpath = Path.joinpath(Path.cwd(), settings.UPLOAD_DIR, fname)

    logger.info(f"Saving spooled file {fname} to {fpath}", filename=fname)
    with open(fpath, "wb") as buffer:
        shutil.copyfileobj(file.file, buffer)

    logger.debug(f"Probing file {fname}")
    (basic_details, raw_details) = probe_file(fpath)
    # Return the saved file details for use elsewhere.
    return {
        "basic_details": {
            "file_name": fname,
            "file_path": fpath,
            "format": basic_details.get("format"),
            "file_size": humanize_size(bytes=fpath.stat().st_size, units="MB"),
            "duration": basic_details.get("duration"),
            "bitrate": basic_details.get("bitrate"),
        },
        "raw_details": raw_details,
    }


def gen_hash(password):
    """Generate hash for a given password

    Args:
        password (str): Plaintext password
    """

    logger.info("Generating hash for password")
    # Bcrypt works with bytes and not strings. So encoding and decoding are necessary
    return bcrypt.hashpw(str(password).encode("utf-8"), bcrypt.gensalt())


def check_pw(password, hash):
    """Checks the given password against the actual password

    Args:
        password (str): The password to check against
        hash (bytes): The hash isn't string but bytes. ;)
    """
    if type(hash) is not bytes:
        hash = hash.encode("utf-8")

    logger.debug("Verifying password hash against given password")
    if bcrypt.checkpw(str(password).encode("utf-8"), hash):
        return True

    # If the control reaches here, then the password is wrong! 😉
    return False


def generate_jwt(payload: dict):
    """Generates a JWT for the given payload. Uses the secret and algorithm from the config object`

    Args:
        payload (dict): A python object for which the JWT will be generated
    """

    logger.debug("Generating JWT")
    token = jwt.encode(payload, key=settings.JWT_SECRET)

    return token


def decode_jwt(token: str): 
    """Given a jwt, decode it as a confirmation of authentication

    Args:
        token (str): base64 encoded jwt string
    """

    try: 
        decoded_jwt = jwt.decode(token, key=settings.JWT_SECRET, algorithms=[settings.JWT_ALGORITHM])
        return decoded_jwt
    except InvalidSignatureError as e:
        # Not an authentic token. 
        logger.debug(f"Invalid JWT encountered", jwt=token)
        return False

class JWTBearer(HTTPBearer):
    """A middleware to be injected as a dependency for authenticating routes. 

    Args:
        HTTPBearer (): Middleware defaulting to FastAPI's http layer. 
    """
    def __init__(self, auto_error: bool = True):
        super(JWTBearer, self).__init__(auto_error=auto_error)

    async def __call__(self, request: Request):
        credentials: HTTPAuthorizationCredentials = await super(JWTBearer, self).__call__(request)
        if credentials:
            verified, payload = self.verify_jwt(credentials.credentials)
            if not credentials.scheme == "Bearer":
                # Bearer token is the HTTP header for authentication. We are concerned with only this. 
                raise HTTPException(status_code=403, detail="Invalid authentication scheme.")
            if not verified:
                logger.debug("Invalid/Expired JWT")
                raise HTTPException(status_code=403, detail="Invalid token or expired token.")
            
            
            from medrip.db_handler import User as UserModel

            user = UserModel.get(email=payload['email'])
            return user
        else:
            raise HTTPException(status_code=403, detail="Invalid authorization code.")

    def verify_jwt(self, jwtoken: str) -> bool:
        isTokenValid: bool = False
        logger.debug(f"Verifying JWT on dependency", jwtoken=jwtoken)
        try:
            payload = decode_jwt(jwtoken)
        except:
            payload = None
        if payload:
            isTokenValid = True
        
        return isTokenValid, payload