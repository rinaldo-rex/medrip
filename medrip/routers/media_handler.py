from logging import log
from fastapi import APIRouter, File, UploadFile, Form, Depends
from fastapi.responses import FileResponse
from starlette.responses import JSONResponse
from medrip.helpers import save_file, JWTBearer
from medrip.ffmpeg_helper import transcode_media
from medrip.db_handler import Media as MediaModel
from medrip.db_handler import Upload as UploadModel
from pydantic import BaseModel
from typing import Optional
from dynaconf import settings
from pathlib import Path
import structlog

logger = structlog.getLogger(__name__)

media_router = APIRouter()

class Media(BaseModel):
    id: int
    original_name: str
    given_name: str


@media_router.post("/files", dependencies=[Depends(JWTBearer())])
def upload_file(
    file: UploadFile = File(...),
    filename: Optional[str] = None,
    transcode: Optional[str] = None,
    raw: bool = False,
    user = Depends(JWTBearer())
):

    logger.info("Uploading file")
    file_info = save_file(file)
    fpath = file_info.get('basic_details')['file_path']
    
    logger.debug(f"Inserting into media: {fpath}")
    media = MediaModel.create(filepath=fpath, original_name=file_info.get('file_name'))
   
    logger.debug(f"Establishing relation between user {user.email} and media: {media.id}")
    UploadModel.create(user=user, media=media)
    
    return {
        "meta_data": {},
        "data": file_info.get("basic_details")
        if not raw
        else file_info.get("raw_details"),
    }


@media_router.get("/files")
def transcode_file(
    filename: Optional[str] = None,
    file: UploadFile = File(...),
    transcode: str = None,
):
    logger.info(f"Uploading file")
    file_info = save_file(file)
    fpath = file_info.get("basic_details").get("file_path")

    if filename is None:
        output_filename = (
            "output." + str(transcode) if transcode else file_info.get("file_name")
        )
    else:
        output_filename = filename + "." + str(transcode)

    output_path = Path.joinpath(Path.cwd(), settings.TRANSCODED_DIR, output_filename)

    logger.debug(f"Transcoding media: {fpath} : to: {transcode} on path: {output_path}")
    transcode_media(input_filepath=fpath, output_filepath=output_path, to=transcode)

    return FileResponse(fpath, filename=output_filename)


@media_router.get("/files/private/download/{file_id}", dependencies=[Depends(JWTBearer())])
def download_private_file(file_id, share_token=None,):

    file = MediaModel.get(id=file_id)

    return FileResponse(file.filepath)


@media_router.get("/files/public/download/{file_id}")
def download_public_file(file_id, share_token=None):
    file = MediaModel.get(id=file_id)
    if file.is_public:
        return FileResponse(file.filepath)
    else:
        return JSONResponse(status_code=401, content={
            "errors":{
                "field": "file_id",
                "message": "You're not authorized to download this file"
            }
        })


@media_router.put("/files/{file_id}")
def share_media(file_id, share_type=(...)):
    file = MediaModel.get(id=file_id)

    logger.info(f"Setting share_type: {share_type}")
    
    if share_type == "public":
        file.is_public = True
        file.save()
    elif share_type == "private":
        file.is_public = False
        file.save()

    return JSONResponse(status_code=200, content={
        "meta": {},
        "data":file.details(),
    })