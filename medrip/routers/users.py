from logging import log
from fastapi import APIRouter, Depends
from fastapi.responses import JSONResponse
from typing import Optional
from pydantic import BaseModel
from medrip.db_handler import User as UserModel
from medrip.db import query_db
from medrip.helpers import check_pw, generate_jwt, JWTBearer
import structlog

logger = structlog.get_logger(__name__)

users_router = APIRouter()


class User(BaseModel):
    id: int
    name: str
    email: str
    is_active: Optional[bool] = True

    # Following are datetime values
    # We can potentially use datetime type for stricter validation
    created_at: Optional[str]
    deactivated_at: Optional[str]
    deleted_at: Optional[str]


class Login(BaseModel):
    email: str
    password: str


class LoginData(BaseModel):
    data: Login
    metadata: Optional[dict]


@users_router.get("/users")
def get_all_users():
    """Get all the available users with minimal details for listing"""
    return {"users": UserModel.get_all_users()}


@users_router.post("/users")
def create_user(user: User):
    pass


@users_router.patch("/users/{user_id}")
def update_user(user_id):
    pass


@users_router.delete("/users/{user_id}")
def delete_user(user_id):
    pass


@users_router.post("/login")
def login_user(login_data: LoginData):
    """Login the user given the email and password"""
    try:
        logger.info(f"Trying to login the user: {login_data.data.email}")
        user = UserModel.get(email=login_data.data.email)
    except UserModel.DoesNotExist:
        logger.warn(f"Unauthorized user login attempt: {login_data.email}")
        return JSONResponse(status_code=404, content = {
            "errors": [{
                "field": "email",
                "message": "User does not exist"
            }]
        })
    
    if check_pw(password=login_data.data.password, hash=user.password_hash):
        # User account matches our database. Generate a JWT for the same.
        # NOTE: JWTs are not encrypted. They are just a way of authentication for the right user. 

        logger.debug(f"Generating access_token for {user.email}")

        token = generate_jwt({
            "id": user.id,
            "email": user.email,
        })

        return JSONResponse(status_code=200, content={
            "data":{
                "access_token": token
            },
            "meta_data":{}
        })
    else:
        logger.debug(f"Invalid password for {user.email}")
        return JSONResponse(status_code=400, content={
            "errors": [{
                "field": "password",
                "message": "Invalid password"
            }]
        })
