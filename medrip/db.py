"""The database manager for a simple read/write using sqlite. 
"""

import sqlite3
import pendulum
from pathlib import Path
import bcrypt
from peewee import SqliteDatabase
import structlog

logger = structlog.getLogger(__name__)

DB_PATH = Path.joinpath(Path.cwd(), "medrip_db.db")


def gen_hash(password):
    """Generate hash for a given password

    Args:
        password (str): Plaintext password
    """
    logger.info("Generating password hash")
    # Bcrypt works with bytes and not strings. So encoding and decoding are necessary
    return bcrypt.hashpw(str(password).encode("utf-8"), bcrypt.gensalt()).decode(
        "utf-8"
    )


# TODO: This should be accessed from a config file
# You don't have to worry about check_same_thread flag here. We're not going to
# sync up the db write operations manually. And moreover, this is not Production db. So chill. 😅
# If you need more info https://stackoverflow.com/a/48234567
medrip_db = sqlite3.connect(DB_PATH, check_same_thread=False)


def query_db(query, cursor=medrip_db.cursor()):
    return cursor.execute(query)


def create_users(cursor, filename=None):
    """Create a list of default users usually on seeding.

    Args:
        filename (string/posixpath, optional): If a filename is provided, the users are created from the file.
                                               Defaults to None.
    """

    # Imported here to avoid circular dependency
    from data import SAMPLE_USERS

    if filename:
        # TODO: Parse values from the file and add the users from the file.
        # For demo purposes, we're skipping this now.
        pass

    for user in SAMPLE_USERS:
        logger.debug(f"Creating user: {user.name}: {user.email}")
        cursor.execute(
            f"""
        INSERT INTO users (name, email, is_active, created_at, password_hash) values (
            "{user.name}", "{user.email}", {1 if user.is_active else 0}, {pendulum.now().timestamp()}, 
            "{gen_hash(user.password)}"
        )"""
        )
    logger.info("Done inserting users")

def create_tables(cursor):
    """Create the default tables using the cursor object.

    Args:
        cursor (Sqlite3 cursor object): The cursor object
    """

    def drop_table(tablename):
        """Drops the table from the database

        Args:
            tablename (str): The name of the table to be dropped
        """
        try:
            cursor.execute(f"""DROP TABLE {tablename}""")
        except sqlite3.OperationalError as e:
            # If the table doesn't exist, it throws an operational error: no such table
            # Log this down
            pass

    drop_table("users")

    # Create users table only if doesn't already exist
    cursor.execute(
        """
        CREATE TABLE IF NOT EXISTS users (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            name TEXT NOT NULL,
            email TEXT NOT NULL UNIQUE,
            password_hash BLOB, 

            created_at BLOB, 
            is_active TEXT, 
            deactivated_at BLOB, 
            deleted_at BLOB
        )
    """
    )

    # Create the media table only if doesn't already exist

    cursor.execute(
        """
        CREATE TABLE IF NOT EXISTS media (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            filepath TEXT NOT NULL,
            original_name TEXT, 
            given_name TEXT
        )
    """
    )


def initialize(db=medrip_db, reseed: bool = True):
    """Initialize the db with tables and base data. Returns

    Args:
        medrip_db (sqlite3 db connection): Sqlite3 db connection object
    """
    cursor = db.cursor()
    from db_handler import User, Media, Upload
    DB_PATH = Path.joinpath(Path.cwd(), "medrip_db.db")
    logger.info(f"Initializing DB to path: {DB_PATH}")

    db= SqliteDatabase(DB_PATH)
    logger.info("Creating tables")
    db.create_tables([User, Media, Upload])
    # create_tables(cursor)
    logger.info("Creating users")
    create_users(cursor)
    logger.debug("Committing DB initialization")
    medrip_db.commit()


if __name__ == "__main__":
    # If called as a script, this script initializes the DB with defaults.
    # TODO: Enable backing up the old db if something exists.
    initialize()
