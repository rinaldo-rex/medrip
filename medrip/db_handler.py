"""
A wrapper manager to handle the db with peewee.
"""

from peewee import Model, SqliteDatabase
from peewee import (
    TextField,
    IntegerField,
    BlobField,
    BooleanField,

    ForeignKeyField,
)
from pathlib import Path
import pendulum
import structlog

logger = structlog.getLogger(__name__)

DB_PATH = Path.joinpath(Path.cwd(), "medrip_db.db")

db = SqliteDatabase(DB_PATH)


class BaseModel(Model):
    class Meta:
        database = db


class User(BaseModel):
    class Meta:
        table_name = "users"

    id = IntegerField(primary_key=True)
    name = TextField()
    email = TextField()
    is_active = BooleanField(default=True)
    password_hash = BlobField()

    # Following are dates/timestamps
    created_at = BlobField()
    deactivated_at = BlobField(null=True)
    deleted_at = BlobField(null=True)

    @classmethod
    def get_all_users(cls):
        from medrip.helpers import humanize_timestamp

        users = User.select()
        all_users_details = []
        logger.debug("Fecthing all users details")
        
        for user in users:
            # print(user)
            all_users_details.append(
                {
                    "id_": user.id,
                    "name": user.name,
                    "email": user.email,
                    "is_active": user.is_active,
                    # Change timestamps to human readable string
                    "created_at": humanize_timestamp(user.created_at),
                    "deactivated_at": humanize_timestamp(user.deactivated_at),
                    "deleted_at": humanize_timestamp(user.deleted_at),
                }
            )
        return all_users_details


class Media(BaseModel):
    class Meta:
        table_name = "media"

    id = IntegerField(primary_key=True)
    filepath=TextField()
    original_name=TextField(null=True)
    given_name= TextField(null=True)

    # Boolean flag to indicate if the media is available private or public.
    is_public = BooleanField(default=False)

    # Share token can be used for extra layer of validation. Such as TOTP. 
    public_share_id = TextField(null=True)
    private_share_id = TextField(null=True)

    # uploaded_by=IntegerField()


    def details(self):
        basic_details = {
            "id": self.id,
            "filepath": self.filepath,
            "original_name": self.original_name,
            "is_public": self.is_public,
            "public_share_id": self.public_share_id,
            "private_share_id": self.private_share_id,
        }
        return basic_details 

class Upload(BaseModel):
    """Relationship table connecting User with Media Upload

    Args:
        BaseModel ([type]): [description]
    """
    
    user = ForeignKeyField(User, backref='uploads')
    media = ForeignKeyField(Media, backref='uploads')
    
