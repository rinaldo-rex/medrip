"""A simple, customized wrapper for ffmpeg and associated libs (Ffprobe, etc)
"""
# For running the shell commands using ffprobe and ffmpeg, through python
import subprocess
import ffmpeg
from ffmpeg import Error as FFMPEGError
from collections import defaultdict
from typing import List
from dynaconf import settings
from pathlib import Path
import shutil
import structlog

logger = structlog.getLogger(__name__)

def sanitize_duration(duration):
    pass


def transcode_media(
    input_filepath,
    output_filepath,
    to: str,
    from_=None,
):
    """Transcode the given media into the "to" format.

    Args:
        file_path (str): The file path. This is usually the path on the server under uploads
        to (str): The format the media will be transcoded to
        from_ (str, optional): The format of the given media. Defaults to None, because this doesn't change anything.
    """
    output_filename = output_filepath.parts[-1]

    logger.debug(f"Transcoding media {output_filename}")

    stdout, stderr = (
        ffmpeg.input(input_filepath)
        .output(output_filename, format=to)
        .run(capture_stdout=True, capture_stderr=True)
    )

    if Path.exists(Path.joinpath(Path.cwd(), output_filename)):
        # File has been transcoded, move to transcoded output directory
        # I had to resort to moving the file since ffmpeg-python's output doesn't 
        # Accept a path for a filename. Lame, or perhaps there's something I'm missing. 
        # Documentation doesn't help. This should work either way. 
        logger.debug(f"Moving decoded media from {output_filename} to {output_filepath}")
        shutil.move(output_filename,output_filepath)

def probe_file(file_path):
    """Probe the given file using ffprobe and return the details of the probe in a readable format.

    Args:
        file_path (str or posixpath): The file path. Either as a string or posixpath
    """
    probe_result = None

    try:
        logger.debug(f"Probing media: {file_path}")
        probe_result = ffmpeg.probe(file_path)

    except FFMPEGError as e:
        logger.error(f"Unable to FFProbe file: {file_path}")
        logger.error(f"Probe error", error=e)
        pass

    # Importing here to avoid circular import error.
    # Architecturally could be improved :TODO:
    from medrip.helpers import humanize_size

    sanitized_probe_result = {}
    if probe_result:
        logger.debug(f"Sanitizing probe result")
        sanitized_probe_result["bitrate"] = humanize_size(
            bytes=probe_result.get("format").get("bit_rate"), units="kbps"
        )
        sanitized_probe_result["format"] = probe_result.get("format").get("format_name")
        # Duration is always in seconds
        sanitized_probe_result["duration"] = round(
            float(probe_result.get("format").get("duration")), 2
        )
        sanitized_probe_result["format"] = probe_result.get("format").get("format_name")

    return dict(sanitized_probe_result), probe_result
